package work.javac;

import work.javac.common.ConfigUtil;
import work.javac.common.JarLoader;
import work.javac.common.Log;
import work.javac.exec.Compare;
import work.javac.exec.Create;
import work.javac.exec.Skewness;
import work.javac.exec.Sync;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

public class Main {

    public static final String USAGE =
            "使用说明: \n" +
                    "\t 同步表: -m sync [-t table操作表名] [-s 卸载数据sql查询语句] [-f file落地数据文件] [-o 写出数据源名] [-i 写入数据源名] \n" +
                    "\t 表结构: -m create [-o 写出数据源名] [-t 表名-支持%模糊匹配] \n" +
                    "\t 数据倾斜: -m skewness [-o 数据源名] [-t 表名-支持%模糊匹配] \n" +
                    "\t 表结构对比: -m compare [-d 多数据源名-以逗号分隔] [-t 表名-支持%模糊匹配] \n" +
            "注: 1. -m 参数不传入,默认sync同步模式; 2.请把数据库驱动包放入jar包同级目录lib中; 3.其余设置请修改config.properties";

    public static void main(String[] args) {
        start(args);
        System.exit(0);
    }

    private static void start(String[] args){
        try {
            JarLoader.init();
            ConfigUtil.init();
        } catch (IOException | InvocationTargetException | NoSuchMethodException | IllegalAccessException e) {
            Log.err(e.getMessage());
            System.exit(1);
        }
        if (args.length == 0) {
            Log.err(USAGE);
            System.exit(1);
        }
        Map<String, String> parse = null;
        try {
            parse = parse(args);
        }catch (RuntimeException e){
            Log.err(USAGE);
            Log.err(e.getMessage());
            System.exit(1);
        }
        if("create".equals(parse.get("mode"))){
            try {
                Create.start(parse);
            } catch (RuntimeException | SQLException | IOException e) {
                Log.err(e.getMessage());
                System.exit(1);
            }
        } else if("skewness".equals(parse.get("mode"))){
            try {
                Skewness.start(parse);
            } catch (RuntimeException | SQLException | IOException e){
                Log.err(e.getMessage());
                System.exit(1);
            }
        } else if("compare".equals(parse.get("mode"))){
            try {
                Compare.start(parse);
            } catch (RuntimeException | SQLException e){
                Log.err(e.getMessage());
                System.exit(1);
            }
        } else {
            try {
                Sync.start(parse);
            } catch (RuntimeException e) {
                Log.err(e.getMessage());
                System.exit(1);
            }
        }
    }

    public static Map<String,String> parse(String[] args) {
        Map<String,String> params = new HashMap<>();
        for (int i = 0; i < args.length; i++) {
            if (args[i].startsWith("-") && args[i].length() == 2) {
                char option = args[i].charAt(1);
                switch(option) {
                    case 'm':
                        params.put("mode", args[++i]);
                        break;
                    case 't':
                        params.put("table", args[++i]);
                        break;
                    case 'f':
                        params.put("file", args[++i]);
                        break;
                    case 'i':
                        params.put("input", args[++i]);
                        break;
                    case 'o':
                        params.put("output", args[++i]);
                        break;
                    case 's':
                        params.put("sql", args[++i]);
                        break;
                    case 'd':
                        params.put("db", args[++i]);
                        break;
                    default:
                        throw new RuntimeException("参数错误: " + args[i]);
                }
            } else throw new RuntimeException("参数错误!");
        }
        return params;
    }


}
