package work.javac.bean;

import java.util.LinkedList;
import java.util.List;

public class Index {

    private String indexName;
    private boolean nonUnique;
    private List<String> columns = new LinkedList<>();

    public String getIndexName() {
        return indexName;
    }

    public void setIndexName(String indexName) {
        this.indexName = indexName;
    }

    public List<String> getColumns() {
        return columns;
    }

    public String getColumn() {
        return columns.get(0);
    }

    public void addColumn(String column) {
        this.columns.add(column);
    }

    public boolean isNonUnique() {
        return nonUnique;
    }

    public void setNonUnique(boolean nonUnique) {
        this.nonUnique = nonUnique;
    }
}
