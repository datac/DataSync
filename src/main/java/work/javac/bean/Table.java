package work.javac.bean;

import java.util.LinkedList;
import java.util.List;

public class Table {

    private String tableName;

    private String remarks;

    private List<Column> columns = new LinkedList<>();

    private String pkName;

    private List<String> primaryKeys = new LinkedList<>();

    private List<Index> indexs = new LinkedList<>();

    private List<Partition> partitions = new LinkedList<>();

    public String getTableName() {
        return tableName;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }

    public List<Column> getColumns() {
        return columns;
    }

    public void addColumn(Column column){
        this.columns.add(column);
    }

    public List<String> getPrimaryKeys() {
        return primaryKeys;
    }

    public void addPrimaryKey(String primaryKey) {
        this.primaryKeys.add(primaryKey);
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getPkName() {
        return pkName;
    }

    public void setPkName(String pkName) {
        this.pkName = pkName;
    }

    public List<Index> getIndexs() {
        return indexs;
    }

    public void addIndex(Index index) {
        if(indexs.size() > 0){
            for(Index forIndex : indexs){
                if(forIndex.getIndexName().equals(index.getIndexName())){
                    forIndex.addColumn(index.getColumn());
                    return;
                }
            }
        }
        indexs.add(index);
    }

    public List<Partition> getPartitions() {
        return partitions;
    }

    public void setPartitions(List<Partition> partitions) {
        this.partitions = partitions;
    }

}
