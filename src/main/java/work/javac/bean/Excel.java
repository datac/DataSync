package work.javac.bean;

import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.metadata.data.WriteCellData;

public class Excel {

    @ExcelProperty(value = "序号", index = 0)
    private int seq;

    @ExcelProperty(value = "英文表名(点击表名可以跳转到详情页)", index = 1)
    private WriteCellData<String> tableName;

    @ExcelProperty(value = "中文表名", index = 2)
    private String Remarks;

    @ExcelProperty(value = "表差异", index = 3)
    private WriteCellData<String> tableExists;

    @ExcelProperty(value = "字段差异", index = 4)
    private WriteCellData<String> columnExists;

    public int getSeq() {
        return seq;
    }

    public void setSeq(int seq) {
        this.seq = seq;
    }

    public WriteCellData<String> getTableName() {
        return tableName;
    }

    public void setTableName(WriteCellData<String> tableName) {
        this.tableName = tableName;
    }

    public WriteCellData<String> getTableExists() {
        return tableExists;
    }

    public void setTableExists(WriteCellData<String> tableExists) {
        this.tableExists = tableExists;
    }

    public WriteCellData<String> getColumnExists() {
        return columnExists;
    }

    public void setColumnExists(WriteCellData<String> columnExists) {
        this.columnExists = columnExists;
    }

    public String getRemarks() {
        return Remarks;
    }

    public void setRemarks(String remarks) {
        Remarks = remarks;
    }
}
