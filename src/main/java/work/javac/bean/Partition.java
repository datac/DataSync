package work.javac.bean;

public class Partition {

    private String columnName;
    private String partitionName;
    private String partitionValueLessThan;

    public String getColumnName() {
        return columnName;
    }

    public void setColumnName(String columnName) {
        this.columnName = columnName;
    }

    public String getPartitionName() {
        return partitionName;
    }

    public void setPartitionName(String partitionName) {
        this.partitionName = partitionName;
    }

    public String getPartitionValueLessThan() {
        return partitionValueLessThan;
    }

    public void setPartitionValueLessThan(String partitionValueLessThan) {
        this.partitionValueLessThan = partitionValueLessThan;
    }
}
