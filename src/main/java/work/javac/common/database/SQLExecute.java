package work.javac.common.database;

import work.javac.bean.Partition;
import work.javac.bean.Table;
import work.javac.common.database.utils.CopyUtil;
import work.javac.common.database.utils.SqlUtil;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

public class SQLExecute {

    public static int executeSQL(Connection conn, String sql) throws SQLException {
        return SQLExecuteBase.executeSQL(conn, sql, null);
    }

    public static int executeSQL(Connection conn, String sql, Object[] datas) throws SQLException {
        return SQLExecuteBase.executeSQL(conn, sql, datas);
    }

    public static String getSchema(Connection conn) throws SQLException {
        return SQLExecuteBase.getSchema(conn);
    }

    public static List<Table> getTables(Connection conn, String table) throws SQLException {
        return SQLExecuteBase.getTables(conn, table);
    }

    public static List<String> getColumns(Connection conn, String tableName) throws SQLException {
        return SQLExecuteBase.getColumns(conn, tableName);
    }

    public static List<Map<String, Object>> executeListMap(Connection conn, SqlUtil util) throws SQLException {
        return SQLExecuteBase.executeListMap(conn, util.getSQL());
    }

    public static long executeCopy(Connection conn, CopyUtil util) throws SQLException {
        return SQLExecuteBase.executeCopy(conn, util);
    }

    public static long executeCopy(Connection fromConn, Connection toConn, CopyUtil util) throws SQLException {
        return SQLExecuteBase.executeCopy(fromConn, toConn, util);
    }

    public static long executeCopyFile(Connection conn, CopyUtil util) throws IOException, SQLException {
        if("I".equals(util.getTYPE())) {
            return SQLExecuteBase.executeCopyFileIn(conn, util);
        }else{
            return SQLExecuteBase.executeCopyFileOut(conn, util);
        }
    }

    public static Map<String, List<Partition>> getPartitions(Connection conn) throws SQLException {
        SQLExecuteBase sqlExecuteBase = SQLExecuteBase.getDBInstance(conn);
        return sqlExecuteBase.getPartitions(conn);
    }

    public static String getViews(Connection conn) throws SQLException {
        SQLExecuteBase sqlExecuteBase = SQLExecuteBase.getDBInstance(conn);
        return sqlExecuteBase.getViews(conn);
    }

    public static List<String> getSkewness(Connection conn, String table) throws SQLException {
        return SQLExecuteBase.getSkewness(conn, table);
    }

    public static List<String> getTableDef(Connection conn, String table) throws SQLException {
        return SQLExecuteBase.getTableDef(conn, table);
    }

}
