package work.javac.common.database.execute;

import work.javac.bean.Partition;
import work.javac.common.Log;
import work.javac.common.database.SQLExecuteBase;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class PostgreSQLExecute extends SQLExecuteBase {

    StringBuffer sql = new StringBuffer();

    @Override
    public String getViews(Connection conn) throws SQLException {
        sql.setLength(0);
        sql.append("SELECT DEFINITION,VIEWNAME FROM PG_VIEWS WHERE SCHEMANAME = '");
        sql.append(getSchema(conn));
        sql.append("'");
        StringBuffer data = new StringBuffer();
        Log.info("开始查询视图信息");
        List<Map<String, Object>> dataList = executeListMap(conn, sql.toString());
        Log.info("涉及视图个数:%s", dataList.size());
        dataList.forEach(item -> {
            data.append("CREATE OR REPLACE VIEW ");
            data.append(item.get("VIEWNAME"));
            data.append(" AS ");
            data.append(item.get("DEFINITION"));
//            data.append(";");
            data.append("\r\n");
        });
        return data.toString();
    }

    @Override
    public Map<String, List<Partition>> getPartitions(Connection conn) throws SQLException {
        return new LinkedHashMap<>();
    }
}
