package work.javac.common.database.execute;

import work.javac.bean.Partition;
import work.javac.common.database.SQLExecuteBase;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class Db2Execute extends SQLExecuteBase {

    StringBuffer sql = new StringBuffer();

    @Override
    public String getViews(Connection conn) throws SQLException {
        return "";
    }

    @Override
    public Map<String, List<Partition>> getPartitions(Connection conn) throws SQLException {
        return new LinkedHashMap<>();
    }

}
