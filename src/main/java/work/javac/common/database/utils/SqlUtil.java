package work.javac.common.database.utils;

public class SqlUtil {

    private StringBuffer SQL = new StringBuffer();

    private String TABLE_NAME;

    private String SCHEMA;

    private StringBuffer COLUMNS = new StringBuffer();

    private StringBuffer WHERE = new StringBuffer();

    private StringBuffer ORDER = new StringBuffer();

    public String getSQL(){
        SQL.setLength(0);
        SQL.append("SELECT ");
        if (COLUMNS.length() == 0){
            SQL.append(" * ");
        }
        SQL.append(COLUMNS);
        SQL.append(" FROM ");
        if(SCHEMA != null){
            SQL.append(SCHEMA);
            SQL.append(".");
        }
        SQL.append(TABLE_NAME);
        if(WHERE.length() > 0){
            SQL.append(WHERE);
        }
        if(ORDER.length() > 0){
            SQL.append(ORDER);
        }
        return SQL.toString();
    }

    public void setColumns(String columns) {
        this.COLUMNS.setLength(0);
        this.COLUMNS.append(columns);
    }

    public void addColumn(String column) {
        if(this.COLUMNS.length() > 0){
            this.COLUMNS.append(", ");
        }
        this.COLUMNS.append(column);
    }

    public void setWhere(String where) {
        this.WHERE.setLength(0);
        this.WHERE.append(where);
    }

    public void setOrder(String order) {
        this.ORDER.setLength(0);
        this.ORDER.append(order);
    }

    public String getTABLE_NAME() {
        return TABLE_NAME;
    }

    public void setTABLE_NAME(String TABLE_NAME) {
        this.TABLE_NAME = TABLE_NAME;
    }

    public String getSCHEMA() {
        return SCHEMA;
    }

    public void setSCHEMA(String SCHEMA) {
        this.SCHEMA = SCHEMA;
    }
}
