package work.javac.common.database.utils;

import work.javac.common.ConfigUtil;

public class CopyUtil {

    private StringBuffer sql = new StringBuffer();

    private String SCHEMA;
    private String TABLENAME;
    private String SQL;
    private StringBuffer COLUMNNAME = new StringBuffer();
    private String DELIMITER = ConfigUtil.get("sync.delimiter");
    private String TYPE = "I";
    private String ENCODING = ConfigUtil.get("sync.encoding");
    private String NULL = ConfigUtil.get("sync.null");
    private String FILEPATH;
    private boolean NOESCAPING = true;
    private String EOL = ConfigUtil.get("sync.eol");
    private StringBuffer data = new StringBuffer();

    public void addData(Object obj){
        data.append(obj);
        data.append(DELIMITER);
    }

    public void endData(){
        data.delete(data.length() - DELIMITER.length(), data.length());
        data.append(EOL);
    }

    public String getData(){
        return data.toString();
    }

    public String getCopySQL(){
        sql.setLength(0);
        sql.append("COPY ");
        if(SQL != null && !"I".equals(TYPE)){
            sql.append("(");
            sql.append(SQL);
            sql.append(")");
        }else{
            if(SCHEMA != null){
                sql.append(SCHEMA);
                sql.append(".");
            }
            sql.append(TABLENAME);
            if(COLUMNNAME.length() > 0){
                sql.append("(");
                sql.append(COLUMNNAME);
                sql.append(")");
            }
        }
        if("I".equals(TYPE)){
            sql.append(" FROM STDIN ");
        }else{
            sql.append(" TO STDOUT ");
        }
        sql.append(" WITH( DELIMITER ");
        sql.append(" E'");
        sql.append(DELIMITER);
        sql.append("'");

        sql.append(" ,ENCODING ");
        sql.append("'");
        sql.append(ENCODING);
        sql.append("'");

        sql.append(" ,NULL ");
        sql.append(" E'");
        sql.append(NULL);
        sql.append("'");

        sql.append(" ,EOL ");
        sql.append(" E'");
        sql.append(EOL);
        sql.append("'");

        sql.append(" ,NOESCAPING '");
        sql.append(NOESCAPING);
        sql.append("' ");

        if("I".equals(TYPE)){
            sql.append(" ,COMPATIBLE_ILLEGAL_CHARS 'TRUE' ");
        }

        sql.append(")");

        return sql.toString();
    }

    public String getTABLENAME() {
        return TABLENAME;
    }

    public void setTABLENAME(String TABLENAME) {
        this.TABLENAME = TABLENAME;
    }

    public void setCOLUMNNAME(String COLUMNNAME) {
        this.COLUMNNAME.setLength(0);
        this.COLUMNNAME.append(COLUMNNAME);
    }

    public void addCOLUMNNAME(String COLUMNNAME) {
        if(this.COLUMNNAME.length() > 0){
            this.COLUMNNAME.append(",");
        }
        this.COLUMNNAME.append(COLUMNNAME);
    }

    public void clearData(){
        this.data.setLength(0);
    }

    public String getFILEPATH() {
        return FILEPATH;
    }

    public void setFILEPATH(String FILEPATH) {
        this.FILEPATH = FILEPATH;
    }

    public String getTYPE() {
        return TYPE;
    }

    public void setTYPE(String TYPE) {
        this.TYPE = TYPE;
    }

    public void setSQL(String SQL) {
        this.SQL = SQL;
    }

    public String getSQL() {
        if(SQL == null){
            return "SELECT * FROM " + TABLENAME;
        }
        return SQL;
    }

    public void setNOESCAPING(boolean NOESCAPING) {
        this.NOESCAPING = NOESCAPING;
    }

    public String getSCHEMA() {
        return SCHEMA;
    }

    public void setSCHEMA(String SCHEMA) {
        this.SCHEMA = SCHEMA;
    }

    public boolean isNOESCAPING() {
        return NOESCAPING;
    }

    public StringBuffer getCOLUMNNAME() {
        return COLUMNNAME;
    }
}
