package work.javac.common;

import java.io.File;
import java.io.InputStream;
import java.nio.file.Paths;

public class PathUtil {

    public static File getFile(String name){
        return Paths.get(getParent(), name).toFile();
    }

    public static String getParent(){
        return new File(PathUtil.class.getProtectionDomain().getCodeSource().getLocation().getFile()).getParent();
    }

    public static InputStream getResourceAsStream(String name){
        return PathUtil.class.getClassLoader().getResourceAsStream(name);
    }

}
