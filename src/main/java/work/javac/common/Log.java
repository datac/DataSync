package work.javac.common;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;

public class Log {

    private static DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.SSS");

    public static void infof(Object... msgs){
        LocalDateTime localDateTime = LocalDateTime.now();
        System.out.print(localDateTime.format(formatter) + "\t");
        if(msgs.length == 1) {
            System.out.print(msgs[0]);
        }else{
            System.out.printf((String) msgs[0], Arrays.copyOfRange(msgs, 1, msgs.length));
        }
    }

    public static void infon(Object... msgs){
        if(msgs.length == 1) {
            System.out.println(msgs[0]);
        }else{
            System.out.printf(msgs[0] + "%n", Arrays.copyOfRange(msgs, 1, msgs.length));
        }
    }

    public static void info(Object... msgs){
        LocalDateTime localDateTime = LocalDateTime.now();
        System.out.print(localDateTime.format(formatter) + "\t");
        if(msgs.length == 1) {
            System.out.println(msgs[0]);
        }else{
            System.out.printf(msgs[0] + "%n", Arrays.copyOfRange(msgs, 1, msgs.length));
        }
    }

    public static void err(Object... msgs){
        if(msgs.length == 1) {
            System.err.println(msgs[0]);
        }else{
            System.err.printf(msgs[0] + "%n", Arrays.copyOfRange(msgs, 1, msgs.length));
        }
    }

}
