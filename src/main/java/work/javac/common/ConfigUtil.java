package work.javac.common;

import java.io.*;
import java.util.Properties;

public class ConfigUtil {

    private static final Properties properties = new Properties();
    private static final String CONFIG_NAME = "config.properties";

    public static void init() {
    }

    static {
        File configFile = PathUtil.getFile(CONFIG_NAME);
        InputStream in = null;
        try {
            if(configFile.exists()){
                in = new FileInputStream(configFile);
            }else{
                in = PathUtil.getResourceAsStream(CONFIG_NAME);
                try (InputStream inputStream = PathUtil.getResourceAsStream(CONFIG_NAME);
                     OutputStream outputStream = new FileOutputStream(configFile)){
                    int i;
                    while ((i = inputStream.read()) != -1){
                        outputStream.write(i);
                    }
                    outputStream.flush();
                }
            }
            properties.load(in);
        } catch (IOException e) {
            Log.err("向%s写入配置文件失败!", configFile.getPath());
            System.exit(1);
        }finally {
            if(in != null){
                try {
                    in.close();
                } catch (IOException e) {
                    Log.err(e.getMessage());
                }
            }
        }
    }

    public static String get(String name){
        return (String) properties.get(name);
    }

}
