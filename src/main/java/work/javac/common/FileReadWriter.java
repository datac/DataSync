package work.javac.common;

import java.io.*;

public class FileReadWriter {

    public static void writer(File filepath, String content) throws IOException {
        try (BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(filepath))) {
            bufferedWriter.write(content);
        }
    }

    public static void writer(String filepath, String content) throws IOException {
        try (BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(filepath))) {
            bufferedWriter.write(content);
        }
    }

    public static String read(String filepath) throws IOException {
        StringBuilder sb = new StringBuilder();
        try (BufferedReader bufferedReader = new BufferedReader(new FileReader(filepath))) {
            String line;
            while((line = bufferedReader.readLine()) != null){
                sb.append(line);
                sb.append(System.getProperty("line.separator"));
            }
        }
        return sb.toString();
    }

    public static String read(InputStream inputStream) throws IOException {
        StringBuilder sb = new StringBuilder();
        try (BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream))) {
            String line;
            while((line = bufferedReader.readLine()) != null){
                sb.append(line);
                sb.append(System.getProperty("line.separator"));
            }
        }
        return sb.toString();
    }

}
